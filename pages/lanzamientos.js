import Head from 'next/head'
import { VerticalTimeline, VerticalTimelineElement }  from 'react-vertical-timeline-component';


export default function lanzamiento({ posts }){
  return(
    <div className="caja">
      <Head>
        <title>Lanzamientos</title>
        <link rel="icon" href="/cohete.ico" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
      </Head>
      
    <main className="">
    <a href="/" className="inicio"><img  src="/home.svg" className="ico-home" /> ir a inicio</a>
    <center><h1>Historico Lanzamientos SPACEX</h1> </center>
        <VerticalTimeline>
  {posts.map((post) => (
  <VerticalTimelineElement
    key={post.flight_number}
    className="vertical-timeline-element--work" 
    date={post.launch_year}
    iconStyle={{ background: 'rgb(33, 150, 243)', color: '#fff' }}
    icon={<center><img  src="/cohete.ico" className="ico-timelane" /></center>}
  >
    <h3 className="vertical-timeline-element-title">{post.mission_name}</h3>
    <ul className="list-group list-group-flush">
       <li className="list-group-item">Lanzamiento: {post.flight_number}</li>
       <li className="list-group-item">Fecha UTC: {post.launch_date_utc}</li>
         <center> <br/> <a id={post.flight_number} type="button" className="btn btn-primary ver-mas" onClick={individual(post.flight_number)} data-toggle="modal" data-target="#myModal">Ver mas</a></center>
    </ul>      
  </VerticalTimelineElement>
  ))}   
</VerticalTimeline>


  <div className="modal fade" id="myModal" role="dialog">
    <div className="modal-dialog modal-lg">
    
      <div className="modal-content">
        <div className="modal-header">
          <h4 className="modal-title" id="titulo">Modal Header</h4>
        </div>
        <div className="modal-body">
        <ul className="list-group list-group-flush">
         <li className="list-group-item" id="tipo"></li>  
         <li className="list-group-item" id="nacionalidad"></li>  
         <li className="list-group-item" id="fabricante"></li> 
         <li className="list-group-item" id="tipoc"></li>  
         <li className="list-group-item" id="orbit"></li>  
         <li  className="list-group-item"><b>Sitio de Lanzamiento</b></li>
         <li className="list-group-item" id="nombre"></li>  
         <li className="list-group-item" id="nombre_largo"></li>  
         <li  className="list-group-item"><b>Detalles</b></li>
         <li className="list-group-item" id="detalles"></li>  
         <li  className="list-group-item"><b>Links</b></li>
         <li className="list-group-item" id="link"></li>
         <li className="list-group-item" id="reddit_campaign"></li>
         <li className="list-group-item" id="reddit_launch"></li>  
        </ul>  
        </div>
        <div className="modal-footer">
          <button type="button" className="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
      </div>
      </div>

  
    </main>
        <style jsx>{`
        main {
          height: 100%;
          // background: #250140;
          background: linear-gradient(45deg, rgba(37,1,64,1) 15%, rgba(139,2,160, 1) 100%) !important;
          // background-image: url("/nebula.jpg");
          // background-repeat: no-repeat;
          // background-size: 100% 100%;
          }
        main h1{
          color: yellow;
          padding-top: 5%;
          padding-bottom: 2%;
        }
        .ico-timelane{
          height: 40%;
          padding-top: 14px;
          top: 40%;
          width: 50%;
        }
        a.inicio{
        top: 10px;
        left: 5px;
        text-decoration: none;
        color: yellow;
        position: fixed;  
        }
        .ico-home{
          height: 20px;
        }
        a.ver-mas{
          color: white !important;
        }
        .modal-title{
          text-transform: uppercase;
        }
        
        @media (max-width: 628px) {
        
        main h1{
          padding-top: 12%;
          font-size: 35px;
        }
        
        a.inicio{
          font-size: 20px;
        }
      }
        
        @media (min-width: 1170px) {
         main {
          width: 100%;
          }
      }
           
      `}</style>
      
    </div>

  )
}


export async function getStaticProps() {
  const res = await fetch('https://api.spacexdata.com/v3/launches/?filter=flight_number,launch_year,mission_name,launch_date_utc&order=desc')
  const posts = await res.json()
  // console.log(posts.length)
  return {
    props: {
      posts,
    },
  }
}




  const individual = (parameter) => (event) => {
  datosindividuales(parameter)
  }

   async function datosindividuales(parameter) {
  const res = await fetch('https://api.spacexdata.com/v3/launches/'+parameter)
  const individualposts = await res.json()
  titulo.innerHTML = individualposts.rocket.rocket_id;
  tipo.innerHTML = 'Tipo: '+individualposts.rocket.rocket_type;
  
  individualposts.rocket.second_stage.payloads.map((leer) => 
  nacionalidad.innerHTML = 'Nacionalidad: '+leer.nationality
   )
  individualposts.rocket.second_stage.payloads.map((leer) => 
  fabricante.innerHTML = 'Fabricante: '+leer.manufacturer
   )
  individualposts.rocket.second_stage.payloads.map((leer) => 
  tipoc.innerHTML = 'Tipo Carga: '+leer.payload_type
   )
  individualposts.rocket.second_stage.payloads.map((leer) => 
  orbit.innerHTML = 'Orbita: '+leer.orbit
   )

   console.log(individualposts.links)
   
   nombre.innerHTML = 'Nombre: '+individualposts.launch_site.site_name
   nombre_largo.innerHTML = 'Nombre largo: '+individualposts.launch_site.site_name_long


   
  
  if(individualposts.details == null){
    detalles.innerHTML = 'No disponible '
  }else{
    detalles.innerHTML = individualposts.details
  }
  if(individualposts.links.mission_patch_small == null){
  link.innerHTML= 'No disponible'  
  }else{
  link.innerHTML= '<center><img src="'+individualposts.links.mission_patch_small+'"  /><center>';
  }

  if(individualposts.links.reddit_campaign == null){
  reddit_campaign.innerHTML= 'No disponible'  
  }else{
  reddit_campaign.innerHTML= '<a href='+individualposts.links.reddit_campaign+'>Hilo Campaña<a>';
  }
  
  if(individualposts.links.reddit_launch == null){
  reddit_launch.innerHTML= 'No disponible'  
  }else{
  reddit_launch.innerHTML= '<a href='+individualposts.links.reddit_launch+'>Hilo Lanzamiento<a>';
  }
       
}