import Head from 'next/head'

import React, { Component } from "react";


export default function mas(){
  return(
    <div className="caja">
      <Head>
        <title>Lanzamientos</title>
        <link rel="icon" href="/cohete.ico" />
      </Head>
    <main >
    <div className="row contenido">
    <div className="col-0 col-sm-0 col-md-0 col-lg-6 col-xl-6 contenido1 animate__animated animate__bounceInLeft">
      <center>
     <img src="/launch.svg" alt="launch" className="img-fluid"></img>
     </center>
    </div>    
    <div className="col-12 col-sm-12 col-md-12 col-lg-5 col-xl-6 contenido2 animate__animated animate__bounceInRight">

         <center >
    <h1></h1> 
    <h2>Space X, es una empresa estadounidense de transporte aeroespacial fundada en 2002 por Elon Musk y busca estar un paso mas cerca a explotar comercialmente los vuelos espaciales.</h2>
   
            <div className="col-7 col-md-6 col-sm-6 col-md-6 col-lg-7 col-xs-3"> <a href="/lanzamientos" className="btn btn-sm animated-button victoria-one">ver lanzamientos</a> </div>
      </center>
    </div>    
    </div>
    </main>
        <style jsx>{`
        main {
          height: 100vh;
          width: 100%;
          background: white;
          background-image: url("/fondo.svg");
          background-repeat: no-repeat;
          background-size: 100% 100%;
          background-size: cover; 
          position: fixed;
          }
        main h1{
          color: blue;
          padding-top: 5%;
          padding-bottom: 2%;
        }
        .contenido{
            padding-top: 5%;
        }
        .contenido1{
            padding-top: 5%;
            padding-left: 6%;
        }
        .contenido2{
            padding-top: 6%;
            z-index: 1;
        }
        .contenido2 center{
            background: rgb(255,255,255,.1);
            padding: 15px;
        }
        .card{
              width: 96% !important;
        }
        .list-group-item{
         text-align: left ; 
        }
        
        @media (min-width: 1440px) {
        .contenido2 h2{
           font-size: 40px;
        }
      }
        @media (max-width: 991px) {
        main {
          height: 100%;
          position: relative;
          }
        .contenido1{
            padding-top: 0%;
        }
        .contenido2{
            padding-top: 0%;
        }
      }

      `}</style>

      
      <style jsx global>{`
        html,
        body {
          padding: 0;
          margin: 0;
          font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Roboto,
            Oxygen, Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue,
            sans-serif;
        }

        * {
          box-sizing: border-box;
        }


a.animated-button:link:after, a.animated-button:visited:after {
	content: "";
	position: absolute;
	height: 0%;
	left: 50%;
	top: 50%;
	width: 100%;
	z-index: -1;
	-webkit-transition: all 0.75s ease 0s;
	-moz-transition: all 0.75s ease 0s;
	-o-transition: all 0.75s ease 0s;
	transition: all 0.75s ease 0s;
}
a.animated-button:hover{
  color: white !important;
}
a.animated-button:link:hover:after, a.animated-button:visited:hover:after {
  
	height: 650%;
}
a.animated-button:link, a.animated-button:visited {
	position: relative;
	display: block;
	margin: 30px auto 0;
	padding: 12px 15px;
	color: #000;
	font-size:14px;
	border-radius: 0;
	font-weight: bold;
	text-align: center;
	text-decoration: none;
	text-transform: uppercase;
	overflow: hidden;
	letter-spacing: .08em;
	text-shadow: 0 0 1px rgba(0, 0, 0, 0.2), 0 1px 0 rgba(0, 0, 0, 0.2);
	-webkit-transition: all 1s ease;
	-moz-transition: all 1s ease;
	-o-transition: all 1s ease;
	transition: all 1s ease;
}

a.animated-button.victoria-one {
  border: 2px solid blue;
  z-index: 999;
}
a.animated-button.victoria-one:after {
	background: blue;
	-moz-transform: translateX(-50%) translateY(-50%) rotate(-25deg);
	-ms-transform: translateX(-50%) translateY(-50%) rotate(-25deg);
	-webkit-transform: translateX(-50%) translateY(-50%) rotate(-25deg);
	transform: translateX(-50%) translateY(-50%) rotate(-25deg);
}
  @media (max-width: 573px) {
 a.animated-button.victoria-one {
  border: 2px solid blue;
  z-index: 999;
}   
a.animated-button.victoria-one:after {
	background: blue;
}
}


        
      `}</style>
    </div>

  )
}


