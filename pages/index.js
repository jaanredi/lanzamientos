import Head from 'next/head'
import Particles from 'react-particles-js'
import Parallax from 'parallax-js'



if (process.browser) {
new Parallax(scene);
// console.log(tsparticles)
(tsparticles).style.height = "100vh";

}


export default function Home() {
  return (
    <div className="caja">
      <Head>
        <title>Lanzamientos</title>
        <link rel="icon" href="/cohete.ico" />
      </Head>

      <main>
      <div className="titulo">
        <h1 className="title">
          Lanzamientos
        </h1>
        <h3>Conoce todas las misiones de spacex</h3>
        <center>
            <div className="col-7 col-md-4 col-sm-4 col-md-4 col-lg-3 col-xs-3"> <a href="/mas" className="btn btn-sm animated-button victoria-one">Aprender más</a> </div>
      </center></div>

      <div className="planetas"  >
      <div id="scene">
  <div data-depth="0.3" className="elements_planetas comet"><img src="/elementos/comet.svg" alt="comet"></img></div>
  <div data-depth="0.4" className="elements_planetas earth"><img src="/elementos/earth.svg" alt="earth"></img></div>
  <div data-depth="0.6" className="elements_planetas ovni"><img src="/elementos/ovni.svg" alt="ovni"></img></div>
  <div data-depth="0.8" className="elements_planetas satellite"><img src="/elementos/satellite.svg" alt="satellite"></img></div>
  <div data-depth="1" className="elements_planetas venus"><img src="/elementos/venus.svg" alt="venus"></img></div>
       </div>
      </div>
      <Particles 
            params={{
  "particles": {
    "number": {
      "value": 100,
      "density": {
        "enable": true,
        "value_area": 800
      }
    },
    "color": {
      "value": "#ffffff"
    },
    "shape": {
      "type": "circle",
      "stroke": {
        "width": 0,
        "color": "#000000"
      },
      "polygon": {
        "nb_sides": 3
      },
      "image": {
        "src": "img/github.svg",
        "width": 100,
        "height": 100
      }
    },
    "opacity": {
      "value": 0.5,
      "random": false,
      "anim": {
        "enable": false,
        "speed": 1,
        "opacity_min": 0.1,
        "sync": false
      }
    },
    "size": {
      "value": 3,
      "random": true,
      "anim": {
        "enable": false,
        "speed": 40,
        "size_min": 0.1,
        "sync": false
      }
    },
    "line_linked": {
      "enable": false,
      "distance": 150,
      "color": "#ffffff",
      "opacity": 0.4,
      "width": 1
    },
    "move": {
      "enable": true,
      "speed": 2,
      "direction": "none",
      "random": false,
      "straight": false,
      "out_mode": "out",
      "bounce": false,
      "attract": {
        "enable": false,
        "rotateX": 600,
        "rotateY": 1200
      }
    }
  },
  "interactivity": {
    "detect_on": "canvas",
    "events": {
      "onhover": {
        "enable": true,
        "mode": "repulse"
      },
      "onclick": {
        "enable": false,
        "mode": "push"
      },
      "resize": true
    },
    "modes": {
      "grab": {
        "distance": 400,
        "line_linked": {
          "opacity": 1
        }
      },
      "bubble": {
        "distance": 400,
        "size": 40,
        "duration": 2,
        "opacity": 8,
        "speed": 3
      },
      "repulse": {
        "distance": 200,
        "duration": 0.4
      },
      "push": {
        "particles_nb": 4
      },
      "remove": {
        "particles_nb": 2
      }
    }
  },
  "retina_detect": true
}}
 />

 
      
      </main> 
     

      <style jsx>{`
        main {
          height: 100vh;
              width: 100%;
          background: #250140;
          background: linear-gradient(45deg, rgba(37,1,64,1) 15%, rgba(139,2,160, 1) 100%) !important;
          position: fixed;
          }
        .titulo{  
             text-align: center!important;
             align-content: center; 
             align-items: center;
             height: 0px;
        }
        .titulo h1{
          font-family:Poppins, sans-serif;
          color: yellow;
          font-size: 80px;
         letter-spacing: 3px;
         padding-top: 15%;
          text-transform: uppercase;
          font-weight: 900;
        }
        .titulo h3{
          font-family:Poppins, sans-serif;
          color: white;
          font-size: 20px;
          text-transform: uppercase;
        }
        .planetas{
          height: 100vh;
          width: 100%;
          position: absolute;
        }
        .elements_planetas{
          width: 100px;
        }
        .comet{
          margin-left : 90% !important;
          margin-top : 3% !important;
        }
        .earth{
          margin-left: 3% !important;
          margin-top: 2% !important;
        }
        .earth{
          margin-left: 3% !important;
          margin-top: 2% !important;
        }
        .ovni{
          margin-left: 40% !important;
          margin-top: 2% !important;
        }
        .satellite{
          margin-left: 20% !important;
          margin-top: 30% !important;
        }
        .venus{
          margin-left: 70% !important;
          margin-top: 25% !important;
        }
        #scene{
          color: white;
        }

        @media (max-width: 768px) {
        .titulo h1{
          font-size: 50px;
         letter-spacing: 3px;
         padding-top: 20%;
          font-weight: 600;
        }
        
        .titulo h3{
          color: white;
          font-size: 14px;
        }
        .comet{
          margin-left : 80% !important;
          margin-top : 3% !important;
        }
        .earth{
          margin-top: 25% !important;
        }
        .ovni{
          margin-left: 40% !important;
          margin-top: 0% !important;
        }
        .satellite{
          margin-left: 25% !important;
          margin-top: 50% !important;
        }
        .venus{
          margin-left: 70% !important;
          margin-top: 35% !important;
        }
}
        @media (max-width: 573px) {
        .titulo h1{
          font-size: 35px;
         letter-spacing: 3px;
         padding-top: 30%;
          font-weight: 600;
        }
        
        .titulo h3{
          color: white;
          font-size: 12px;
        }
        .earth{
          margin-top: 5% !important;
        }
        .ovni{
          margin-left: 50% !important;
          margin-top: 60% !important;
        }
        .satellite{
          margin-left: 10% !important;
          margin-top: 100% !important;
        }
        .venus{
          margin-left: 70% !important;
          margin-top: 95% !important;
        }
}
        

        



      `}</style>

      <style jsx global>{`
        html,
        body {
          padding: 0;
          margin: 0;
          font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Roboto,
            Oxygen, Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue,
            sans-serif;
        }

        * {
          box-sizing: border-box;
        }


a.animated-button:link:after, a.animated-button:visited:after {
	content: "";
	position: absolute;
	height: 0%;
	left: 50%;
	top: 50%;
	width: 100%;
	z-index: -1;
	-webkit-transition: all 0.75s ease 0s;
	-moz-transition: all 0.75s ease 0s;
	-o-transition: all 0.75s ease 0s;
	transition: all 0.75s ease 0s;
}
a.animated-button:link:hover:after, a.animated-button:visited:hover:after {
	height: 650%;
}
a.animated-button:link, a.animated-button:visited {
	position: relative;
	display: block;
	margin: 30px auto 0;
	padding: 12px 15px;
	color: #fff;
	font-size:14px;
	border-radius: 0;
	font-weight: bold;
	text-align: center;
	text-decoration: none;
	text-transform: uppercase;
	overflow: hidden;
	letter-spacing: .08em;
	text-shadow: 0 0 1px rgba(0, 0, 0, 0.2), 0 1px 0 rgba(0, 0, 0, 0.2);
	-webkit-transition: all 1s ease;
	-moz-transition: all 1s ease;
	-o-transition: all 1s ease;
	transition: all 1s ease;
}

a.animated-button.victoria-one {
  border: 2px solid rgb(255, 255, 0, .9);
  z-index: 999;
}
a.animated-button.victoria-one:after {
	background: rgb(255, 255, 0, .6);
	-moz-transform: translateX(-50%) translateY(-50%) rotate(-25deg);
	-ms-transform: translateX(-50%) translateY(-50%) rotate(-25deg);
	-webkit-transform: translateX(-50%) translateY(-50%) rotate(-25deg);
	transform: translateX(-50%) translateY(-50%) rotate(-25deg);
}
  @media (max-width: 573px) {
 a.animated-button.victoria-one {
  border: 2px solid blue;
  z-index: 999;
}   
a.animated-button.victoria-one:after {
	background: blue;
}
}


        
      `}</style>
    </div>
  )
}








