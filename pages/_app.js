import React from 'react'

import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/css/bootstrap-grid.min.css';
import 'animate.css/animate.min.css';

import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css";

import 'react-vertical-timeline-component/style.min.css';


export default function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />
}